// --------------------
//! @file
// see wiki: http://arrizza.org/wiki/index.php/ArduinoFastSquare

//! using this #include is a bad idea!
// it's called "_private" because it should stay
// unused except for those folks working on the Arduino Library
// itself, but... we're going to do it anyway because we need to
// set the pin state as fast as we can.
// cppcheck-suppress missingInclude
#include "wiring_private.h"

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    // initialize the digital pin as an output.
    pinMode(10, OUTPUT);
}

// --------------------
//! toggle the pin routines, skip the Arduino library
// and call directly into the AVR code
// Note: the cbi/sbi routines are not
// function calls, they are macros
// cppcheck-suppress unusedFunction
void loop()
{
    // Normally runs at around 2.000Mhz,
    // assuming the ATmega clock is 16Mhz.
    // Oscilloscope reading: 1.98823Mhz
    for (;;) {
        // Clear Bit: set pin 10 low
        cbi(PORTB, PORTB2);

        // compensate for the jump instruction in the for() loop
        // so the output is a square wave.
        // the sei and cbi take 2 clock cycles each,
        // each NOP is 1 clock cycle,
        // the jump takes 2 clock cycles
        // so: 4 clocks high, 4 clocks low
        // 16Mz / 8 cycles = 2Mhz expected frequency
        __asm__("nop" ::);
        __asm__("nop" ::);

        // add nops here to slow the frequency down
        // __asm__ ("nop" ::);

        // Set Bit: set pin 10 high
        sbi(PORTB, PORTB2);

        // add the same number of nops here
        // to maintain a square wave
        // __asm__ ("nop" ::);
    }
}
